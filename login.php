<style>
 .modal-content {
      width: 80%;
      margin: 0 auto;
    }
    .modal-body {
      padding: 0;
    }
    .btn-close {
      position: absolute;
      right: 0;
      padding: 1em;
    }
    .la {
      font-size: 2.3em;
      font-weight: bold;
    }
    .myform {
      padding: 2em;
      max-width: 100%;
      color: #fff;
      box-shadow: 0 4px 6px 0 rgba(22, 22, 26, 0.18);
    }
    @media (max-width: 576px) {
      .myform {
        max-width: 90%;
        margin: 0 auto;
      }
    }
    .form-control:focus {
      box-shadow: inset 0 -1px 0 #7e7e7e;
    }
    .form-control {
      background-color: inherit;
      color: #fff;
      padding-left: 0;
      border: 0;
      border-radius: 0;
      border-bottom: 1px solid #fff;
    }
    .myform .btn {
      width: 100%;
      font-weight: 800;
      background-color: #fff;
      border-radius: 0;
      padding: 0.5em 0;
    }
     
    .myform .btn:hover {
      background-color: inherit;
      color: #fff;
      border-color: #fff;
    }
    .lp {
      text-align: center;
      padding-top: 2em;
      color: #fff;
    }
    .lp button {
      color: black;
      text-decoration: none;
     
      width:30px;
    }
    .lp button:hover {
      color: #fff;
    }
    
  </style>

<div class="modal fade" id="ModalForm" tabindex="-1" aria-labelledby="ModalFormLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content ">
        <div class="modal-body lmodal-body">
            <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
            <div class="myform bg-dark" >
                <h1 class="la text-center">Login Form</h1>

                <form action="?" method="post">
                    <div class="mb-3 mt-4">
                        <label for="exampleInputEmail1" class="form-label">Email address</label>
                        <input type="email" class="form-control " name="email"  aria-describedby="emailHelp"
                        oninvalid="InvalidMsg4(this);"  oninput="InvalidMsg4(this);" required>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">Password</label>
                        <input type="password" name="password" class="form-control " 
                        oninvalid="InvalidMsg2(this);"  oninput="InvalidMsg2(this);" required>
                    </div>
                    <button type="submit"  name="submit1" class="btn btn-light mt-3">LOGIN</button>
                    <p class="lp">Not a member? 
                    <button type="button" name="submit"  class="btn btn-light  btn-sm " data-bs-toggle="modal" 
                    data-bs-target="#signup">SIGN UP</button>
                    </p>
                </form>
            </div>
        </div>
      </div>
    </div>
</div>

<?php      
if (session_status() === PHP_SESSION_NONE) {
    session_start();
};
$prod = true;
		  if ($prod == true) {
        $conn = new mysqli("containers-us-west-98.railway.app", "root", "durbuH34C8GvPwOkRbHT", "railway", "5625");
		  } else {
        $conn = new mysqli('localhost', 'id20527066_root', 'qvL~p#(TK?N0WZdq', 'id20527066_ezdeb');
		  }
if($conn->connect_error) {
  echo"<script> alert('Something error occured');</script>";
} 
 if(isset($_POST['submit1'])){
    if (isset($_SESSION['id'])) {
        echo"<script> alert('already logined');</script>"; 
       // <script type="text/javascript"> window.setTimeout(function() { window.location.href='adproduct.php'; }, 1000); </script>
         header('Refresh:0.5;index.php');
        die();
      }
    else{ $username = $_POST['email'];  
     $password = $_POST['password'];
    $username = stripcslashes($username);  //to prevent from mysqli injection    
    $password = stripcslashes($password);  
    $username = mysqli_real_escape_string($conn, $username);  
    $password = mysqli_real_escape_string($conn, $password);  
    $h_password = md5($password);
    $sql = "SELECT *from users where email = '$username' AND  password='$h_password' AND type=1";
    $result = mysqli_query($conn, $sql);  
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);  
    $count = mysqli_num_rows($result);  
    
    if($count == 1){  
       $_SESSION['id'] =$username;
       echo "<script> alert('Login successful');</script>";
      // header('Refresh:0.1; index.php');?>
      <script> location.replace("index.php"); </script>
    <?php
    }
    else{  
       echo "<script> alert('Login failed');</script>";
     // header('Refresh:0.4; index.php');
     echo ("<script>location.href = 'index.php';</script>");
       die();
    }}}   ?>  

