<!---footer section-->
<section id="footer">
<img src="images/wave2.png" class="footer-img">
<div class="container">
    <div class="row">
        <div class="col-md-4 footer-box">
            <img src="images/demo-logo.png" class="footer-img">  
            <p>Manage Debian packages from GitHub and websites with ease.</p>
        </div>
        <div class="col-md-4 footer-box">
        </div>
        <div class="col-md-4 footer-box">
            <br>
           <p><b>Get help</b></p>
           <p><a style="color: inherit;" href="bot.php">Chat with Bot</p>
           <p><a style="color: inherit;" href="guide/index.php">Read the guide</p>
           <p><a style="color: inherit;" href="https://ezdeb.flarum.cloud">Discuss on forum</a></p>
        </div>

    </div>
</div>
<hr><p class="copyright">website created by EZDEB team</p>
</section>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"
 integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
 
  <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="js/plugins/plugins.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>
    
</html>
