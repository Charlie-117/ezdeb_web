<!DOCTYPE html>
<html lang="en"> 
<head>
    <title>EZDEB - Help</title>
    
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    
    <!-- FontAwesome JS-->
    <script defer src="assets/fontawesome/js/all.min.js"></script>

    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="assets/css/help_theme.css">

	<!-- Navbar -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
      <link rel="stylesheet" href="../css/font-awesome.min.css">
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css"> 
      <link rel='stylesheet' href='../style.css' />  

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" 
integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    

</head> 

<body>    

    <!--  Navbar -->

	<section id="nav-bar">  
        <nav class="navbar navbar-expand-lg bg-body-tertiary navbar-dark" >
            <div class="container-fluid">
              <a class="navbar-brand" href="../index.php">EZDEB</a> 
<!-- /images/demo-logo1.png-->
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" 
              aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
              <!--class="navbar-toggler-icon"navbar-toggler -->
              </button>
              <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ms-auto" >
                  <li class="nav-item">
                    <a style="color:#fff;" class="nav-link "  href="../index.php">Home</a>
                  </li>
                  <li class="nav-item">
                    <a style="color:#fff;"class="nav-link" href="index.php">Installation</a>
                  </li>
                  <li class="nav-item">
                    <a style="color:#fff;" class="nav-link" href="../packagelist.php">Packages</a>
                  </li>
                 <!-- <li class="nav-item">
                    <a class="nav-link" href="#" >Services</a>
                  </li>-->
                  
                  <li class="nav-item">
                    <a style="color:#fff;" class="nav-link" href="../bot.php">Chat with us</a>
                  </li>
                  <li class="nav-item">
                    <a style="color:#fff;" class="nav-link" href="https://ezdeb.flarum.cloud">Forum</a>
                  </li>
                  <li class="nav-item">
                    <a style="color:#fff;" class="nav-link "href="../about.php">About us</a>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
    </section>


<!-- Navbar end -->
    
    <div class="page-header theme-bg-dark py-5 text-center position-relative">
	    <div class="theme-bg-shapes-right"></div>
	    <div class="theme-bg-shapes-left"></div>
	    <div class="container">
		    <h1 class="page-heading single-col-max mx-auto">EZDEB</h1>
		    <div class="page-intro single-col-max mx-auto">Manage Debian packages from GitHub and websites with ease.</div>
	    </div>
    </div><!--//page-header-->
   <div class="page-content">
	    <div class="container">
		    <div class="docs-overview py-5">
			    <div class="row justify-content-center">
				    <div class="col-12 col-lg-4 py-3">
					    <div class="card shadow-sm">
						    <div class="card-body">
							    <h5 class="card-title mb-3">
								    <span class="theme-icon-holder card-icon-holder me-2">
								        <i class="fas fa-tools"></i>
							        </span><!--//card-icon-holder-->
							        <span class="card-title-text">Build from source</span>
							    </h5>
							    <div class="card-text">
								    Guide to get you started on building EZDEB binary from source.
							    </div>
							    <a class="card-link-mask" href="docs-page.php#build-section"></a>
						    </div><!--//card-body-->
					    </div><!--//card-->
				    </div><!--//col-->
				    <div class="col-12 col-lg-4 py-3">
					    <div class="card shadow-sm">
						    <div class="card-body">
							    <h5 class="card-title mb-3">
								    <span class="theme-icon-holder card-icon-holder me-2">
								        <i class="fas fa-arrow-down"></i>
							        </span><!--//card-icon-holder-->
							        <span class="card-title-text">Installation</span>
							    </h5>
							    <div class="card-text">
								    Guide for installing EZDEB on your computer.
							    </div>
							    <a class="card-link-mask" href="docs-page.php#install-section"></a>
						    </div><!--//card-body-->
					    </div><!--//card-->
				    </div><!--//col-->
				    <div class="col-12 col-lg-4 py-3">
					    <div class="card shadow-sm">
						    <div class="card-body">
							    <h5 class="card-title mb-3">
								    <span class="theme-icon-holder card-icon-holder me-2">
								        <i class="fas fa-book-reader"></i>
							        </span><!--//card-icon-holder-->
							        <span class="card-title-text">Usage guide</span>
							    </h5>
							    <div class="card-text">
								    Usage guide for using EZDEB, includes help for all commands.					    
								</div>
							    <a class="card-link-mask" href="docs-page.php#usage-section"></a>
						    </div><!--//card-body-->
					    </div><!--//card-->
				    </div><!--//col-->
			    </div><!--//row-->
		    </div><!--//container-->
		</div><!--//container-->
    </div><!--//page-content-->

    <footer class="footer">

	    <!-- TODO: replace with footer -->
	    
    </footer>
       
    <!-- Javascript -->          
    <script src="assets/plugins/popper.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>  
    
    <!-- Page Specific JS -->
    <script src="assets/plugins/smoothscroll.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.8/highlight.min.js"></script>
    <script src="assets/js/highlight-custom.js"></script> 
    <script src="assets/plugins/simplelightbox/simple-lightbox.min.js"></script>      
    <script src="assets/plugins/gumshoe/gumshoe.polyfills.min.js"></script> 
    <script src="assets/js/docs.js"></script> 

</body>
</html> 

