<?php
include('navbar.php');
include('login.php');
include('signup.php');
?>

<style>
  .team .member {
position: relative;
box-shadow: 0px 2px 15px rgba(0, 0, 0, 0.1);
padding: 30px;
border-radius: 5px;
background: #fff;
}

.team .member .teampic {
overflow: hidden;
width: 180px;
border-radius: 50%;
}

/* .team .member .teampic img {
transition: ease-in-out 0.3s;
} */


.team .member .member-info {
padding-left: 30px;
}

.team .member h4 {
font-weight: 700;
margin-bottom: 5px;
font-size: 20px;
}

.team .member span {
display: block;
font-size: 15px;
padding-bottom: 10px;
position: relative;
font-weight: 500;
}

.team .member span::after {
content: "";
position: absolute;
display: block;
width: 50px;
height: 2px;
background: black;
bottom: 0;
left: 0;
}

.team .member p {
margin: 10px 0 0 0;
font-size: 14px;
}

.team .member .social {
margin-top: 12px;
display: flex;
align-items: center;
justify-content: flex-start;
}

.team .member .social a {
display: flex;
align-items: center;
justify-content: center;
width: 32px;
height: 32px;
}

.team .member .social a i {
font-size: 16px;
margin: 0 2px;
}

.team .member .social a+a {
margin-left: 8px;
}
</style>

<div class="asection">
        <div class="acontainer">
            <div class="acontent-section">

            <div class="atitle">
                <h1>About Us</h1>
            </div>
            <div class="acontent">
                <h3>
                Manage Debian packages from GitHub and websites with ease.</h3>
                     <p>Our team developed EZDEB, a CLI app for managing debian packages.
                       It's accompanied by a website and forum, providing easy access to package information, ratings, and discussions.
                       <br> The project was created as a major project for our degree course, with a focus on community needs and solving real-world problems.</p>
                <div class="button">
                    <a href="#team">Meet our Team</a>
                </div>
            </div>
           
        </div>
    
        <div class="aimage-section">
            <img src="images/network.png" >
        </div>
   
</div>
</div>

<!-- ======= Team Section ======= -->
<section id="team" class="team section-bg">
        <div class="container">
  
          <div class="section-title">
          <br><br>
            <h2>Our Team</h2>
            <div class="underline"></div>
            <p></p>
          </div>
  
          <div class="row">
  
            <div class="col-lg-6">
              <div class="member d-flex align-items-start" >
                <div class="teampic"><img src="https://png.pngtree.com/png-vector/20220817/ourmid/pngtree-cartoon-man-avatar-vector-ilustration-png-image_6111064.png" class="img-fluid" alt=""></div>
                <div class="member-info">
                  <h4>Tony Jose</h4>
                  <span>Roll number: 50</span>
                  <p>BCA graduate</p>
                  <div class="social">
                    <a href="https://www.linkedin.com/in/tonyjose02/"><i class="bi bi-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
  
            <div class="col-lg-6 mt-4 mt-lg-0">
              <div class="member d-flex align-items-start" >
                <div class="teampic"><img src="https://png.pngtree.com/png-clipart/20221207/ourmid/pngtree-business-man-avatar-png-image_6514640.png" class="img-fluid" alt=""></div>
                <div class="member-info">
                  <h4>Murali Krishna</h4>
                  <span>Roll number: 37</span>
                  <p>BCA graduate</p>
                  <div class="social">
                    <a href="https://www.linkedin.com/in/murali-krishna-4a68a1265"><i class="bi bi-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
  
            <div class="col-lg-6 mt-4">
              <div class="member d-flex align-items-start" >
                <div class="teampic"><img src="https://png.pngtree.com/png-vector/20220628/ourmid/pngtree-male-profile-avatar-user-portrait-png-image_5548548.png" class="img-fluid" alt=""></div>
                <div class="member-info">
                  <h4>Christin Varghese</h4>
                  <span>Roll number: 21</span>
                  <p>BCA graduate</p>
                  <div class="social">
                    <a href=""><i class="bi bi-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
  
            <div class="col-lg-6 mt-4">
              <div class="member d-flex align-items-start" >
                <div class="teampic"><img src="https://w7.pngwing.com/pngs/309/479/png-transparent-saxophone-clarinet-avatar-computer-icons-afro-child-face-hand-thumbnail.png" class="img-fluid" alt=""></div>
                <div class="member-info">
                  <h4>Shine B John</h4>
                  <span>Roll number: 44</span>
                  <p>BCA graduate</p>
                  <div class="social">
                    <a href=""><i class="bi bi-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
  
          </div>
  
        </div>
      </section><!-- End Team Section -->

<?php
include('footer.php');
?>